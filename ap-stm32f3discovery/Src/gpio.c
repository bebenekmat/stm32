/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

void MX_GPIO_Init(void)
{
    __HAL_RCC_GPIOE_CLK_ENABLE();
    GPIO_InitTypeDef gpio;
    gpio.Pin = GPIO_PIN_14 | GPIO_PIN_15 | GPIO_PIN_13 | GPIO_PIN_12 | GPIO_PIN_11 | GPIO_PIN_10 | GPIO_PIN_9 | GPIO_PIN_8;
    gpio.Mode = GPIO_MODE_OUTPUT_PP;
    gpio.Pull = GPIO_NOPULL;
    gpio.Speed = GPIO_SPEED_FREQ_LOW;

    HAL_GPIO_Init(GPIOE, & gpio);


}
void MX_GPIO_diod(uint8_t state, uint8_t diod)
{
  if(state==1)
  {
     HAL_GPIO_WritePin(GPIOE, 1 << diod, GPIO_PIN_SET);
  }
  else
  {
     HAL_GPIO_WritePin(GPIOE, 1 << diod, GPIO_PIN_RESET);
  }
}
void MX_GPIO_diods(uint8_t state)
{
    int i;
    if(state == 1)
    {
        for(i = 8; i < 16; i++)
        {
            HAL_GPIO_WritePin(GPIOE, 1 << i, GPIO_PIN_SET);
            HAL_Delay(100);
        }
    }
    else
    {
        for(i = 15; i >=8 ; i--)
        {
            HAL_GPIO_WritePin(GPIOE, 1 << i, GPIO_PIN_RESET);
            HAL_Delay(100);
        }
    }
}
void MX_GPIO_blink(void)
{
    int i;
    for(i = 8; i < 16; i++)
    {
        HAL_GPIO_WritePin(GPIOE, 1 << i, GPIO_PIN_SET);
    }
    HAL_Delay(500);
    for(i = 8; i < 16; i++)
    {
        HAL_GPIO_WritePin(GPIOE, 1 << i, GPIO_PIN_RESET);
    }
    HAL_Delay(500);
}
void MX_GPIO_circle(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
    HAL_GPIO_WritePin(GPIOE, 1 << a, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, 1 << b, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, 1 << c, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, 1 << d, GPIO_PIN_SET);
    HAL_Delay(25);
    HAL_GPIO_WritePin(GPIOE, 1 << a, GPIO_PIN_RESET);
    HAL_Delay(25);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
