/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define BUFFER_SIZE 32
char* buffer;
uint8_t counter = 0;
UART_HandleTypeDef huart1;

char* info;
char info2[256];
//void (*slave)(uint8_t, uint8_t) = &MX_GPIO_diod;

void MX_USART1_UART_Init(void)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;

  HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);

  HAL_UART_Init( & huart1);
  buffer = malloc(BUFFER_SIZE);
  HAL_UART_Receive_IT(&huart1, (uint8_t*)buffer, 1);
}


void HAL_UART_MspInit(UART_HandleTypeDef * uartHandle)
{
  __HAL_RCC_USART1_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  GPIO_InitTypeDef gpio;
  gpio.Pin = GPIO_PIN_4 | GPIO_PIN_5;
  gpio.Pull = GPIO_PULLUP;
  gpio.Mode = GPIO_MODE_AF_OD;
  gpio.Alternate = GPIO_AF7_USART1;
  gpio.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOC, & gpio);
}

uint8_t compare(const char* tab, uint8_t size)
{
    int i = 0;
    for(i = 0; i < size; i++)
    {
        if(buffer[i+1]!=tab[i])
        {
            return 1;
        }
    }
    return 0;
}

void handleDiods()
{
    if(compare("diod on", 7)==0)
    {
        if(buffer[counter-1]=='1')
        {
          info = "\r\nenabling diod 1\r\n";
          MX_GPIO_diod(1, 8);
        }
        else if(buffer[counter-1]=='2')
        {
          info = "\r\nenabling diod 2\r\n";
          MX_GPIO_diod(1,9);
        }
        else if(buffer[counter-1]=='3')
        {
          info = "\r\nenabling diod 3\r\n";
          MX_GPIO_diod(1,10);
        }
        else if(buffer[counter-1]=='4')
        {
          info = "\r\nenabling diod 4\r\n";
          MX_GPIO_diod(1,11);
        }
        else if(buffer[counter-1]=='5')
        {
          info = "\r\nenabling diod 5\r\n";
          MX_GPIO_diod(1,12);
        }
        else if(buffer[counter-1]=='6')
        {
          info = "\r\nenabling diod 6\r\n";
          MX_GPIO_diod(1,13);
        }
        else if(buffer[counter-1]=='7')
        {
          info = "\r\nenabling diod 7\r\n";
          MX_GPIO_diod(1,14);
        }
        else if(buffer[counter-1]=='8')
        {
          info = "\r\nenabling diod 8\r\n";
          MX_GPIO_diod(1,15);
        }
        else
        {
           info = "\r\nenabling all diods\r\n";
           flag=1;
        }


    }
    else if(compare("diod off", 8) == 0)
    {
        if(buffer[counter-1]=='1')
        {
          info = "\r\ndisabling diod 1\r\n";
          MX_GPIO_diod(0, 8);
        }
        else if(buffer[counter-1]=='2')
        {
          info = "\r\ndisabling diod 2\r\n";
          MX_GPIO_diod(0, 9);
        }
        else if(buffer[counter-1]=='3')
        {
          info = "\r\ndisabling diod 3\r\n";
          MX_GPIO_diod(0, 10);
        }
        else if(buffer[counter-1]=='4')
        {
          info = "\r\ndisabling diod 4\r\n";
          MX_GPIO_diod(0,11);
        }
        else if(buffer[counter-1]=='5')
        {
          info = "\r\ndisabling diod 5\r\n";
          MX_GPIO_diod(0,12);
        }
        else if(buffer[counter-1]=='6')
        {
          info = "\r\ndisabling diod 6\r\n";
          MX_GPIO_diod(0,13);
        }
        else if(buffer[counter-1]=='7')
        {
          info = "\r\ndisabling diod 7\r\n";
          MX_GPIO_diod(0,14);
        }
        else if(buffer[counter-1]=='8')
        {
          info = "\r\ndisabling diod 8\r\n";
          MX_GPIO_diod(0,15);
        }
        else
        {
           info = "\r\ndisabling all diods\r\n";
           flag = 2;
        }
    }
    else if(compare("diod blink",10) == 0)
    {
        info = "\r\nenabling blinking\r\n";
        flag = 3;
    }
    else if(compare("diod stop", 9) == 0)
    {
        info = "\r\nstoping all actions\r\n";
        flag = 0;
    }
    else if(compare("diod circle", 11) == 0)
    {
        info = "\r\nenabling diod circle\r\n";
        flag = 4;
    }
    else
    {
        info = "\r\n\npossible commands:"
               "\r\ndiod on\t\t\t- enable all diods"
               "\r\ndiod on [1-8]\t\t- enable specyfic diod"
               "\r\ndiod off\t\t- disable all diods"
               "\r\ndiod off [1-8]\t\t- disable specyfic diod"
               "\r\ndiod blink\t\t- all diods blinking"
               "\r\ndiod stop\t\t- stopping all actions"
               "\r\ndiod circle\t\t- diods are going around"
               "\r\n";
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef * huart1)
{

    if(buffer[counter] == 13)
    {
       if(compare("diod",4) == 0)
       {
            handleDiods();
       }
       else if(compare("help", 4) == 0)
       {
           info = "\r\n\npossible commands:\r\ndiod [args]  \t\t- write diod to see all options\r\n";

       }
       else if(compare("math",4) == 0)
       {
           int result=0;
           char delimiter[] = " ";
           char* ptr;
           int num1;
           int num2;
           ptr = strtok(buffer, delimiter);
           ptr = strtok(NULL,delimiter);
           num1 = atoi(ptr);
           ptr = strtok(NULL, delimiter);
           num2 = atoi(ptr);
           ptr = strtok(NULL, delimiter);
           char* operation = ptr;
           char arr[10] = "";
           if(strstr(operation, "add") != NULL)
           {
               result = num1 + num2;
           }
           else if(strstr(operation, "sub") != NULL)
           {
               result = num1 - num2;

           }
           else if(strstr(operation, "multi") != NULL)
           {
               result = num1 * num2;
           }
           else if(strstr(operation, "div") != NULL)
           {
               result = num1 / num2;
           }
           itoa(result,arr,10);
           char* a = "\r\nYour result is:";
           HAL_UART_Transmit(huart1, (uint8_t*)a, strlen(a), 1000);
           HAL_UART_Transmit(huart1, (uint8_t*)arr, 10, 1000);
           info= "\r\nHope i helped You\r\n";
       }
       else
       {
           info = "\r\nunknown command\r\n";
       }
       HAL_UART_Transmit_IT(huart1, (uint8_t*)info, strlen(info));
       info = "";
       counter = 0;
    }
    HAL_UART_Transmit_IT(huart1, (uint8_t*)&buffer[counter], 1);
    counter++;
    HAL_UART_Receive_IT(huart1,  (uint8_t*)&buffer[counter], 1);

}

void HAL_UART_MspDeInit(UART_HandleTypeDef * uartHandle)
{
}




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
